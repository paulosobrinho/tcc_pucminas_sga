# README #

This README would normally document whatever steps are necessary to get your application up and running.

# Objetivo do Trabalho #

O objetivo deste trabalho é apresentar uma solução de melhoria no processo de gestão ambiental através da 
implantação ou melhoria de um sistema de informação para a gestão ambiental apresentando a descrição do projeto 
arquitetural de uma aplicação adequada para este sistema.



# Objetivos da Solução #

	A solução visa atender aspectos de segurança, disponibilidade e principalmente a fácil manutenibilidade do SGA, 
	visto que, modificações na legislação ambiental, o desenvolvimento tecnológico contribuem para uma maior frequência 
	de mudanças nas regras de negócios e processos do SGA. Esta solução de software visa atender 2 demandas da empresa, 
	gestão de transporte de materiais e gestão de energia elétrica. A solução arquitetural visa atender este aspecto como
	o mais importante dentro de uma empresa que adequa as práticas ISO 14001 e a legislação vigente local. A aplicação 
	será capaz de executar e armazenar regras de negócios e processos diferentes para cada filial da empresa. A solução
	arquitetural proposta para atender esta realidade é um sistema com sua arquitetura baseada em microsserviços.
	
# Apresentação do Problema #

Atualmente empresas de grande porte tem enfrentado problemas em manter sistemas de software pela dificuldade de 
mutabilidade e desempenho insatisfatório que as arquiteturas de software legada quando existe algum sistema de software.
 A automação no processo de informações em companhia de qualquer natura aumenta a produtividade e reduz custos, não sendo
 diferente para um cenário de gestão ambiental, neste sentido além de se adequar a politicas governamentais um sistema 
 que automatiza informações de processos de diferentes áreas pode tornar mais efetiva o processo de adequação ambiental
 das empresas, visto que o controle de informações é algo centralizado de forma simples e eficiente para o usuário final, 
 assim como é a proposta de qualquer produto de software, eliminando trabalhos manuais, preenchimento de planilhas e 
 documentos desnecessários para controle de estoque e descarte adequado de materiais. Desta forma transpondo todas essas 
 informações para um sistema de gestão ambiental além de significar um menor custo no trabalho de gestão ‘manual” no 
 processamento destas informações torna o processo mais ágil. Isso significa em um exemplo ilustrativo que uma companhia 
 de minério de ferro por exemplo por duplicar o transporte de rejeitos por dia por conta de um sistema de gestão ambiental,
 quando antes este trabalho era administrado através de pranchetas e planilhas e de forma não centralizada, dificultando 
 ainda mais a gestão. Por outro lado, não apenas informatizar a companhia com um SGA , as arquitetura  de sistemas legados 
 em grande parte são monolíticas de difícil manutenção. Mas até mesmo arquitetura de serviços caso não seja bem desenvolvida
 pode não ser tão eficiente no quesito manutenibilidade, a depender do desenho arquitetural pode impactar em outras
 funcionalidades. Portanto, a manutenibilidade é um quesito importante para sistemas de gerenciamento ambiental, como 
 comentando anteriormente. Pois regras de negócios e processos são modificados constantemente. O problema não consiste
 apenas em implantar um sistema de gestão ambiental qualquer, mas sim definir uma arquitetura de software adequada para
 este tipo de negócio através de artefatos de software, código fontes e comprovações técnicas justificando a arquitetura 
 de software e com menções a qualidade ao adotar a especificação sugerida na aplicação para este tipo de sistema de software. 
 Será utilizada tecnologias web tendo em vista a alta gama de compatibilidade e facilidade de acesso do sistema.


