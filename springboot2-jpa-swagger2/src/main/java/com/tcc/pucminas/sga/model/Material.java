package com.tcc.pucminas.sga.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "material", schema ="pucminas")
@ApiModel(description="Todos os detalhes da conta bancária ")
public class Material {

	@ApiModelProperty(notes = "ID  com autoincrement")
	private long idMaterial;

	@ApiModelProperty(notes = "ID do usuario")
	private long idColetor;

	@ApiModelProperty(notes = "Saldo da conta")
	private BigDecimal saldo;

	@ApiModelProperty(notes = "Limite da conta")
	private BigDecimal limiteSaqueDiario;

	@ApiModelProperty(notes = "Flag da conta ativa")
	private Long flagAtivo;

	@ApiModelProperty(notes = "Tipo da Material")
	private Long tipoMaterial;

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	@ApiModelProperty(notes = "Data de criação da conta")
	private Date dataCriacao;

	public Material() { }

	public Material(long idMaterial, long idColetor, BigDecimal saldo, BigDecimal limiteSaqueDiario,
					Long flagAtivo, Long tipoMaterial, Date dataCriacao) {
		this.idColetor = idColetor;
		this.saldo = saldo;
		this.idMaterial = idMaterial;
		this.limiteSaqueDiario = limiteSaqueDiario;
		this.flagAtivo = flagAtivo;
		this.tipoMaterial = tipoMaterial;
		this.dataCriacao = dataCriacao;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_material", nullable = false)
	public long getIdMaterial() {
		return idMaterial;
	}

	public void setIdMaterial(long id) {
		this.idMaterial = id;
	}

	@Column(name = "id_coletor", nullable = false)
	public long getIdColetor() {
		return idColetor;
	}

	public void setIdColetor(long idColetor) {
		this.idColetor = idColetor;
	}

	@Column(name = "saldo", nullable = false)
	public BigDecimal getSaldo() {
		return saldo;
	}

	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}

	@Column(name = "limite_saque_diario", nullable = false)
	public BigDecimal getLimiteSaqueDiario() {
		return limiteSaqueDiario;
	}

	public void setLimiteSaqueDiario(BigDecimal limiteSaqueDiario) {
		this.limiteSaqueDiario = limiteSaqueDiario;
	}

	@Column(name = "flag_ativo", nullable = false)
	public Long isFlagAtivo() {
		return flagAtivo;
	}

	public void setFlagAtivo(Long flagAtivo) {
		this.flagAtivo = flagAtivo;
	}

	@Column(name = "tipo_conta", nullable = false)
	public Long getTipoMaterial() {
		return tipoMaterial;
	}

	public void setTipoMaterial(Long tipoMaterial) {
		this.tipoMaterial = tipoMaterial;
	}

	@Column(name = "data_criacao", nullable = false)
	public Date getDataCriacao() {
		return dataCriacao;
	}


	@Override
	public String toString() {
		return "Material [id=" + idMaterial + ", idColetor=" + idColetor + ", saldo=" + saldo + ", limiteSaqueDiario="
				+ limiteSaqueDiario  + ", flagAtivo=" + flagAtivo + ", tipoMaterial=" + tipoMaterial +  ", dataCriacao="
				+ dataCriacao.toString() + "]";
	}

}
