package com.tcc.pucminas.sga.controller;

import io.swagger.annotations.*;
import com.tcc.pucminas.sga.exception.ResourceNotFoundException;
import com.tcc.pucminas.sga.interfaceService.IColetorService;
import com.tcc.pucminas.sga.model.Coletor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/sga/coletor")
@Api(value="Coletor SGA  - Management System", description="Operaçoes do cliente")
public class ColetorController {

    @Autowired
    private IColetorService coletorService;

    @ApiOperation(value = "Cadastrar Cliente")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Operação Realizada com Sucesso."),
            @ApiResponse(code = 400, message = "Formato incompatível."),
            @ApiResponse(code = 401, message = "Você não possui autorização para acessar este recurso."),
            @ApiResponse(code = 403, message = "Acesso bloqueado ao recurso solicitado."),
            @ApiResponse(code = 404, message = "O recurso que esta tentando acessar não está disponível."),
            @ApiResponse(code = 500, message = "Ocorreu um erro em sua solicitação. Tente mais tarde.")})
    @PostMapping("/criar")
    public Coletor createMaterial(
            @ApiParam(value = "Armazenar uma nova coletor", required = true)
            @Valid @RequestBody Coletor coletor) throws Exception {

        return coletorService.save(coletor);
    }


    @ApiOperation(value = "Obter coletor por Id ")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Operação Realizada com Sucesso."),
            @ApiResponse(code = 400, message = "Formato incompatível."),
            @ApiResponse(code = 401, message = "Você não possui autorização para acessar este recurso."),
            @ApiResponse(code = 403, message = "Acesso bloqueado ao recurso solicitado."),
            @ApiResponse(code = 404, message = "O recurso que esta tentando acessar não está disponível."),
            @ApiResponse(code = 500, message = "Ocorreu um erro em sua solicitação. Tente mais tarde.")})
    @GetMapping("/idColetor/{idColetor}")
    public ResponseEntity<Coletor> getColetorById(
            @ApiParam(value = "ID da coletor da qual o objeto da coletor será recuperado", required = true)
            @PathVariable(value = "idColetor") Long idColetor)
            throws ResourceNotFoundException {

        return ResponseEntity.ok().body(coletorService.findByIdColetor(idColetor));
    }
}

