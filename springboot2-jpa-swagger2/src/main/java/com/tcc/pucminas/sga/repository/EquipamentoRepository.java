package com.tcc.pucminas.sga.repository;

import com.tcc.pucminas.sga.model.Equipamento;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EquipamentoRepository extends JpaRepository<Equipamento, Long>{

}
