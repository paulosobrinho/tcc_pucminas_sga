package com.tcc.pucminas.sga.interfaceService;

import com.tcc.pucminas.sga.exception.ResourceNotFoundException;
import com.tcc.pucminas.sga.model.Equipamento;

import java.util.List;


public interface IEquipamentoService {

    List<Equipamento> findAll();

    Equipamento findByIdEquipamento(Long idEquipamento) throws ResourceNotFoundException;

    Equipamento save(Equipamento equipamento) throws ResourceNotFoundException;


}
