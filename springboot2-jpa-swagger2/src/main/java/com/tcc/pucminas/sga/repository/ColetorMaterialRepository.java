package com.tcc.pucminas.sga.repository;

import com.tcc.pucminas.sga.model.ColetorMaterial;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ColetorMaterialRepository extends JpaRepository<ColetorMaterial, Long> {
}
