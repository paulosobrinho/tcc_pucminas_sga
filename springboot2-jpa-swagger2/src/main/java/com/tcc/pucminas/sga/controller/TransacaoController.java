package com.tcc.pucminas.sga.controller;

import io.swagger.annotations.*;
import com.tcc.pucminas.sga.exception.ResourceNotFoundException;
import com.tcc.pucminas.sga.model.Transacao;
import com.tcc.pucminas.sga.service.TransacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/sga/transacao")
@Api(value="Transacoes SGA  - Management System", description="Transacoes de conta bancária")
public class TransacaoController {

    @Autowired
    TransacaoService transacaoService;

    @ApiOperation(value = "Visualizar a lista de  transacoes de uma conta", response = List.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Operação Realizada com Sucesso."),
            @ApiResponse(code = 400, message = "Formato incompatível."),
            @ApiResponse(code = 401, message = "Você não possui autorização para acessar este recurso."),
            @ApiResponse(code = 403, message = "Acesso bloqueado ao recurso solicitado."),
            @ApiResponse(code = 404, message = "O recurso que esta tentando acessar não está disponível."),
            @ApiResponse(code = 500, message = "Ocorreu um erro em sua solicitação. Tente mais tarde.")})
    @GetMapping("/listarByIdMaterial/{idMaterial}")
    public List<Transacao> getTransacoesByIdMaterial(
            @ApiParam(value = "ID da conta da qual o objeto da Transacao será recuperado", required = true)
            @PathVariable(value = "idMaterial") Long idMaterial)
            throws ResourceNotFoundException {

        return transacaoService.findByIdMaterial(idMaterial);
    }

    @ApiOperation(value = "Visualizar a lista de  transacoes de uma conta (Remessas)", response = List.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Operação Realizada com Sucesso."),
            @ApiResponse(code = 400, message = "Formato incompatível."),
            @ApiResponse(code = 401, message = "Você não possui autorização para acessar este recurso."),
            @ApiResponse(code = 403, message = "Acesso bloqueado ao recurso solicitado."),
            @ApiResponse(code = 404, message = "O recurso que esta tentando acessar não está disponível."),
            @ApiResponse(code = 500, message = "Ocorreu um erro em sua solicitação. Tente mais tarde.")})
    @GetMapping("/listarByIdMaterialByPeriodo/{idMaterial}")
    public List<Transacao> getTransacoesByIdMaterial(
            @ApiParam(value = "ID da conta da qual o objeto da Transacao será recuperado", required = true)
            @PathVariable(value = "idMaterial") Long idMaterial,
            @PathVariable(value = "periodoInicial") Date periodoInicial,
            @PathVariable(value = "periodoFinal") Date periodoFinal)
            throws ResourceNotFoundException {

        return transacaoService.findByIdMaterial(idMaterial);
    }
}
