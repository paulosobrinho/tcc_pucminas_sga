package com.tcc.pucminas.sga.interfaceService;

import com.tcc.pucminas.sga.model.Transacao;

import java.util.List;

public interface ITransacaoService {

    Transacao save(Transacao transacao);

    List<Transacao> findByIdMaterial(long idMaterial);

}
