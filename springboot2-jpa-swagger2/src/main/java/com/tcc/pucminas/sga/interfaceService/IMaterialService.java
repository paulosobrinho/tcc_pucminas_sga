package com.tcc.pucminas.sga.interfaceService;

import com.tcc.pucminas.sga.exception.ResourceNotFoundException;
import com.tcc.pucminas.sga.model.Material;

import java.math.BigDecimal;
import java.util.List;


public interface IMaterialService  {

    List<Material> findAll();

    Material findByIdMaterial(Long idMaterial) throws ResourceNotFoundException;

    Material desativarMaterialByIdMaterial(Long idMaterial) throws ResourceNotFoundException;

    Material ativarMaterialByIdMaterial(Long idMaterial) throws ResourceNotFoundException;

    String findSaldoByIdMaterial(Long idMaterial) throws ResourceNotFoundException;

    String saqueByIdMaterial(Long idMaterial, BigDecimal valorDebitar) throws ResourceNotFoundException, Exception;

    String depositoByIdMaterial(Long idMaterial, BigDecimal valorCreditar) throws ResourceNotFoundException, Exception;

    Material save(Material material) throws ResourceNotFoundException;

    Material updateMaterial(Long idMaterial, Material materialDetails) throws ResourceNotFoundException;

}
