package com.tcc.pucminas.sga.repository;

import com.tcc.pucminas.sga.model.Coletor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ColetorRepository extends JpaRepository<Coletor, Long>{

    List<Coletor> findByCpfCnpj(String cpfcnpj);
}
