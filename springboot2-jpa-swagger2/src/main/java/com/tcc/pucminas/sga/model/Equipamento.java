package com.tcc.pucminas.sga.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "equipamento", schema ="pucminas")
@ApiModel(description="Todos os detalhes da conta bancária ")
public class Equipamento {

	@ApiModelProperty(notes = "ID  com autoincrement")
	private long idEquipamento;

	@ApiModelProperty(notes = "Flag da conta ativa")
	private Long flagAtivo;

	@ApiModelProperty(notes = "Tipo de Equipamento")
	private Long tipoEquipamento;

	@ApiModelProperty(notes = "Consumo de Energia")
	private Long energiaKwh;

	@ApiModelProperty(notes = "Setor Id")
	private Long setorId;

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	@ApiModelProperty(notes = "Data de criação da conta")
	private Date dataCriacao;

	public Equipamento() { }

	public Equipamento(long idEquipamento, Long flagAtivo, Long tipoEquipamento, Date dataCriacao) {
		this.idEquipamento = idEquipamento;
		this.flagAtivo = flagAtivo;
		this.tipoEquipamento = tipoEquipamento;
		this.energiaKwh = energiaKwh;
		this.dataCriacao = dataCriacao;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_equipamento", nullable = false)
	public long getIdEquipamento() {
		return idEquipamento;
	}

	public void setIdEquipamento(long id) {
		this.idEquipamento = id;
	}

	@Column(name = "flag_ativo", nullable = false)
	public Long isFlagAtivo() {
		return flagAtivo;
	}

	public void setFlagAtivo(Long flagAtivo) {
		this.flagAtivo = flagAtivo;
	}

	@Column(name = "tipo_conta", nullable = false)
	public Long getTipoEquipamento() {
		return tipoEquipamento;
	}

	@Column(name = "consumo_energia", nullable = true)
	public Long getEnergiaKwh() {
		return energiaKwh;
	}

	public void setEnergiaKwh(Long energiaKwh) { this.energiaKwh = energiaKwh;}

	@Column(name = "setor_identificador", nullable = true)
	public Long getSetorId() {
		return setorId;
	}

	public void setSetorId(Long setorId) { this.setorId = setorId;}

	public void setTipoEquipamento(Long tipoEquipamento) {
		this.tipoEquipamento = tipoEquipamento;
	}

	@Column(name = "data_criacao", nullable = false)
	public Date getDataCriacao() {
		return dataCriacao;
	}




	@Override
	public String toString() {
		return "Equipamento [id=" + idEquipamento + ", flagAtivo=" + flagAtivo + ", tipoEquipamento=" + tipoEquipamento +  ", dataCriacao="
				+ dataCriacao.toString() + "]";
	}

}
