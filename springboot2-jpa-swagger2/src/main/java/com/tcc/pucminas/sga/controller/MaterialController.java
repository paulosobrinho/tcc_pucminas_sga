package com.tcc.pucminas.sga.controller;

import com.tcc.pucminas.sga.interfaceService.IMaterialService;
import io.swagger.annotations.*;
import com.tcc.pucminas.sga.exception.ResourceNotFoundException;
import com.tcc.pucminas.sga.model.Material;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping("/sga/material")
@Api(value="Material SGA  - Management System", description="Operaçoes de material bancária")
public class MaterialController {

	@Autowired
	private IMaterialService materialService;

	@ApiOperation(value = "Visualizar a lista de materiais", response = List.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operação Realizada com Sucesso."),
			@ApiResponse(code = 400, message = "Formato incompatível."),
			@ApiResponse(code = 401, message = "Você não possui autorização para acessar este recurso."),
			@ApiResponse(code = 403, message = "Acesso bloqueado ao recurso solicitado."),
			@ApiResponse(code = 404, message = "O recurso que esta tentando acessar não está disponível."),
			@ApiResponse(code = 500, message = "Ocorreu um erro em sua solicitação. Tente mais tarde.")})
	@GetMapping("/listar")
	public List<Material> getAllMaterials() {
		return materialService.findAll();
	}

	@ApiOperation(value = "Obter material por Id - consulta limite de saque e Saldo")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operação Realizada com Sucesso."),
			@ApiResponse(code = 400, message = "Formato incompatível."),
			@ApiResponse(code = 401, message = "Você não possui autorização para acessar este recurso."),
			@ApiResponse(code = 403, message = "Acesso bloqueado ao recurso solicitado."),
			@ApiResponse(code = 404, message = "O recurso que esta tentando acessar não está disponível."),
			@ApiResponse(code = 500, message = "Ocorreu um erro em sua solicitação. Tente mais tarde.")})
	@GetMapping("/numeroMaterial/{idMaterial}")
	public ResponseEntity<Material> getMaterialById(
			@ApiParam(value = "ID da material da qual o objeto da material será recuperado", required = true)
			@PathVariable(value = "idMaterial") Long idMaterial)
			throws ResourceNotFoundException {

		return ResponseEntity.ok().body(materialService.findByIdMaterial(idMaterial));
	}

	@ApiOperation(value = "Obter saldo da material por Id - consulta limite de saque e Saldo")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operação Realizada com Sucesso."),
			@ApiResponse(code = 400, message = "Formato incompatível."),
			@ApiResponse(code = 401, message = "Você não possui autorização para acessar este recurso."),
			@ApiResponse(code = 403, message = "Acesso bloqueado ao recurso solicitado."),
			@ApiResponse(code = 404, message = "O recurso que esta tentando acessar não está disponível."),
			@ApiResponse(code = 500, message = "Ocorreu um erro em sua solicitação. Tente mais tarde.")})
	@GetMapping("/saldo/numeroMaterial/{idMaterial}")
	public String getSaldoMaterialById(
			@ApiParam(value = "ID da material da qual o objeto da material será recuperado", required = true)
			@PathVariable(value = "idMaterial") Long idMaterial)
			throws ResourceNotFoundException {

		return materialService.findSaldoByIdMaterial(idMaterial);
	}

	@ApiOperation(value = "Realiza o saque da material por IdMaterial")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operação Realizada com Sucesso."),
			@ApiResponse(code = 400, message = "Formato incompatível."),
			@ApiResponse(code = 401, message = "Você não possui autorização para acessar este recurso."),
			@ApiResponse(code = 403, message = "Acesso bloqueado ao recurso solicitado."),
			@ApiResponse(code = 404, message = "O recurso que esta tentando acessar não está disponível."),
			@ApiResponse(code = 500, message = "Ocorreu um erro em sua solicitação. Tente mais tarde.")})
	@PutMapping("/saqueValor/{valorDebitar}/numeroMaterial/{idMaterial}")
	public String saqueMaterialById(
			@ApiParam(value = "Valor de Depósito e Numero da material que será efetuado o Saque", required = true)
			@PathVariable(value = "idMaterial") Long idMaterial,
			@PathVariable(value = "valorDebitar") BigDecimal valorDebitar)
			throws Exception {

		return materialService.saqueByIdMaterial(idMaterial, valorDebitar);
	}

	@ApiOperation(value = "Realiza o deposito em uma material por IdMaterial")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operação Realizada com Sucesso."),
			@ApiResponse(code = 400, message = "Formato incompatível."),
			@ApiResponse(code = 401, message = "Você não possui autorização para acessar este recurso."),
			@ApiResponse(code = 403, message = "Acesso bloqueado ao recurso solicitado."),
			@ApiResponse(code = 404, message = "O recurso que esta tentando acessar não está disponível."),
			@ApiResponse(code = 500, message = "Ocorreu um erro em sua solicitação. Tente mais tarde.")})
	@PutMapping("/depositoValor/{valorCreditar}/numeroMaterial/{idMaterial}")
	public String depositoMaterialById(
			@ApiParam(value = "Valor de Depósito e Numero da material que será efetuado o Deposito", required = true)
			@PathVariable(value = "idMaterial") Long idMaterial,
			@PathVariable(value = "valorCreditar") BigDecimal valorCreditar)
			throws Exception {

		return materialService.depositoByIdMaterial(idMaterial, valorCreditar);
	}

	@ApiOperation(value = "Criar material")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operação Realizada com Sucesso."),
			@ApiResponse(code = 400, message = "Formato incompatível."),
			@ApiResponse(code = 401, message = "Você não possui autorização para acessar este recurso."),
			@ApiResponse(code = 403, message = "Acesso bloqueado ao recurso solicitado."),
			@ApiResponse(code = 404, message = "O recurso que esta tentando acessar não está disponível."),
			@ApiResponse(code = 500, message = "Ocorreu um erro em sua solicitação. Tente mais tarde.")})
	@PostMapping("/criar")
	public Material createMaterial(
			@ApiParam(value = "Armazenar uma nova material", required = true)
			@Valid @RequestBody Material material) throws ResourceNotFoundException {

		return materialService.save(material);
	}

	@ApiOperation(value = "Atualizar material")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operação Realizada com Sucesso."),
			@ApiResponse(code = 400, message = "Formato incompatível."),
			@ApiResponse(code = 401, message = "Você não possui autorização para acessar este recurso."),
			@ApiResponse(code = 403, message = "Acesso bloqueado ao recurso solicitado."),
			@ApiResponse(code = 404, message = "O recurso que esta tentando acessar não está disponível."),
			@ApiResponse(code = 500, message = "Ocorreu um erro em sua solicitação. Tente mais tarde.")})
	@PutMapping("/atualizar/{idMaterial}")
	public ResponseEntity<Material> updateMaterial(
			@ApiParam(value = "Material Id para atualizar a partir do modelo fornecido", required = true)
			@PathVariable(value = "idMaterial") Long idMaterial,
			@ApiParam(value = "Atualizar objeto material", required = true)
			@Valid @RequestBody Material materialDetails) throws ResourceNotFoundException {

		return ResponseEntity.ok(materialService.updateMaterial(idMaterial, materialDetails));
	}


	@ApiOperation(value = "Desativar uma material por idMaterial")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Desativado. Operação Realizada com Sucesso."),
			@ApiResponse(code = 400, message = "Formato incompatível."),
			@ApiResponse(code = 401, message = "Você não possui autorização para acessar este recurso."),
			@ApiResponse(code = 403, message = "Acesso bloqueado ao recurso solicitado."),
			@ApiResponse(code = 404, message = "O recurso que esta tentando acessar não está disponível."),
			@ApiResponse(code = 500, message = "Ocorreu um erro em sua solicitação. Tente mais tarde.")})
	@PutMapping("/desativar/numeroMaterial/{idMaterial}")
	public Material desativarMaterialByIdMaterial(
			@ApiParam(value = "ID da material da qual o objeto da material será recuperado", required = true)
			@PathVariable(value = "idMaterial") Long idMaterial)
			throws ResourceNotFoundException {

		return materialService.desativarMaterialByIdMaterial(idMaterial);
	}

	@ApiOperation(value = "Ativar uma material por idMaterial")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Ativado. Operação Realizada com Sucesso."),
			@ApiResponse(code = 400, message = "Formato incompatível."),
			@ApiResponse(code = 401, message = "Você não possui autorização para acessar este recurso."),
			@ApiResponse(code = 403, message = "Acesso bloqueado ao recurso solicitado."),
			@ApiResponse(code = 404, message = "O recurso que esta tentando acessar não está disponível."),
			@ApiResponse(code = 500, message = "Ocorreu um erro em sua solicitação. Tente mais tarde.")})
	@PutMapping("/ativar/numeroMaterial/{idMaterial}")
	public Material ativarrMaterialByIdMaterial(
			@ApiParam(value = "ID da material da qual o objeto da material será recuperado", required = true)
			@PathVariable(value = "idMaterial") Long idMaterial)
			throws ResourceNotFoundException {

		return materialService.ativarMaterialByIdMaterial(idMaterial);
	}
}
