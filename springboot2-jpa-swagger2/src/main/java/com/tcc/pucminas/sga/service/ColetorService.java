package com.tcc.pucminas.sga.service;

import com.tcc.pucminas.sga.exception.ResourceNotFoundException;
import com.tcc.pucminas.sga.interfaceService.IColetorService;
import com.tcc.pucminas.sga.repository.ColetorRepository;
import com.tcc.pucminas.sga.model.Coletor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ColetorService implements IColetorService {

    @Autowired
    private ColetorRepository coletorRepository;

    /**
     *
     * @param coletor
     * @return Coletor
     */
    public Coletor save(Coletor coletor) throws  Exception{
        List<Coletor> listColetor = new ArrayList<>();
        listColetor.addAll(coletorRepository.findByCpfCnpj(coletor.getCpfCnpj()));
        if(listColetor.size() > 0){
              throw new Exception("Já existe uma coletor cadastrada com o CPF informado.");
        }

        coletor.setIdColetor(0);
        return coletorRepository.save(coletor);
    }

    /**
     *
     * @param idColetor
     * @return Coletor
     * @throws ResourceNotFoundException
     */
    public Coletor findByIdColetor(Long idColetor) throws ResourceNotFoundException {
        return  coletorRepository.findById(idColetor)
                .orElseThrow(() -> new ResourceNotFoundException("Coletor não encontrada para o id: " + idColetor));
    }

    /**
     *
     * @param cpfcnpj
     * @return List<Coletor>
     */
    public List<Coletor> findByCpfCnpj(String cpfcnpj) {
        return  coletorRepository.findByCpfCnpj(cpfcnpj);

    }

}
