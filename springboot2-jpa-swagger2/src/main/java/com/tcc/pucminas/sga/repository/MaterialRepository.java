package com.tcc.pucminas.sga.repository;

import com.tcc.pucminas.sga.model.Material;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MaterialRepository extends JpaRepository<Material, Long>{

}
