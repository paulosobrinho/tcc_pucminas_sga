package com.tcc.pucminas.sga.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "coletor", schema ="pucminas")
@ApiModel(description="Todos os detalhes do cliente da conta ")
public class Coletor {

    @ApiModelProperty(notes = "ID do usuario")
    private long idColetor;

    @ApiModelProperty(notes = "Nome da coletor")
    private String nome;

    @ApiModelProperty(notes = "CpfCnpj da coletor")
    private String cpfcnpj;

    @ApiModelProperty(notes = "Data de Cadastro da coletor")
    private Date dataCadastro;

    public Coletor() { }



    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_coletor", nullable = false)
    public long getIdColetor() {
        return idColetor;
    }

    public void setIdColetor(long idColetor) {
        this.idColetor = idColetor;
    }

    @Column(name = "nome", nullable = false)
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Column(name = "cpfcnpj", nullable = false)
    public String getCpfCnpj() {
        return cpfcnpj;
    }

    public void setCpfCnpj(String cpfcnpj) {
        this.cpfcnpj = cpfcnpj;
    }

    @Column(name = "data_cadastro", nullable = false)
    public Date getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    @Override
    public String toString() {
        return "Coletor [idColetor=" + idColetor + ", nome=" + nome + ", cpfcnpj=" + cpfcnpj + ", dataCadastro="
                + dataCadastro  + "]";
    }

}
