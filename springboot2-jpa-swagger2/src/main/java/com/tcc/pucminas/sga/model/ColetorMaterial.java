package com.tcc.pucminas.sga.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

@Entity
@Table(name = "coletor_conta", schema ="pucminas")
@ApiModel(description="Tabela de Relacionamento entre coletor e conta ")
public class ColetorMaterial {

    @ApiModelProperty(notes = "ID  com autoincrement")
    private long idColetorMaterial;

    @ApiModelProperty(notes = "ID do usuario")
    private long idMaterial;

    @ApiModelProperty(notes = "ID do usuario")
    private long idColetor;

    public ColetorMaterial(){}

    public ColetorMaterial(long idColetorMaterial, long idMaterial, long idColetor) {
        this.idColetorMaterial = idColetorMaterial;
        this.idMaterial = idMaterial;
        this.idColetor = idColetor;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_coletor_conta", nullable = false)
    public long getIdColetorMaterial() {
        return idColetorMaterial;
    }

    public void setIdColetorMaterial(long idColetorMaterial) {
        this.idColetorMaterial = idColetorMaterial;
    }

    @Column(name = "id_material", nullable = false)
    public long getIdMaterial() {
        return idMaterial;
    }

    public void setIdMaterial(long idMaterial) {
        this.idMaterial = idMaterial;
    }

    @Column(name = "id_coletor", nullable = false)
    public long getIdColetor() {
        return idColetor;
    }

    public void setIdColetor(long idColetor) {
        this.idColetor = idColetor;
    }
}
