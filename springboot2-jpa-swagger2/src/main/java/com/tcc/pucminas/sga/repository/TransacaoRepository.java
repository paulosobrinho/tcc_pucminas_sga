package com.tcc.pucminas.sga.repository;

import com.tcc.pucminas.sga.model.Transacao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransacaoRepository extends JpaRepository<Transacao, Long> {

    List<Transacao> findByIdMaterial(long idMaterial);

}
