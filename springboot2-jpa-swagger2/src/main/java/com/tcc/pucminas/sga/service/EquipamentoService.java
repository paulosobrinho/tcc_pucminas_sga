package com.tcc.pucminas.sga.service;

import com.tcc.pucminas.sga.exception.ResourceNotFoundException;
import com.tcc.pucminas.sga.interfaceService.IEquipamentoService;
import com.tcc.pucminas.sga.model.Coletor;
import com.tcc.pucminas.sga.model.Equipamento;
import com.tcc.pucminas.sga.repository.EquipamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class EquipamentoService implements IEquipamentoService {

    @Autowired
    private EquipamentoRepository contaRepository;

    @Autowired
    private  ColetorService coletorService;

    @Autowired
    private  TransacaoService transacaoService;

    public List<Equipamento> findAll(){
        return contaRepository.findAll();
    }

    /**
     *
     * @param idEquipamento
     * @return Equipamento
     * @throws ResourceNotFoundException
     */
    public Equipamento findByIdEquipamento(Long idEquipamento) throws ResourceNotFoundException {
        return  contaRepository.findById(idEquipamento)
                .orElseThrow(() -> new ResourceNotFoundException("Equipamento não encontrada para o id: " + idEquipamento));
    }


    /**
     *
     * @param equipamento
     * @return Equipamento
     */
    public Equipamento save(Equipamento equipamento) throws ResourceNotFoundException {
        Coletor coletor = new Coletor();
        Equipamento equipamentoNova = new Equipamento();
        equipamentoNova =  contaRepository.save(equipamento);
        return equipamentoNova;
    }

    /**
     *
     * @param idEquipamento
     * @param equipamentoDetails
     * @return Equipamento
     * @throws ResourceNotFoundException
     */
    public Equipamento updateEquipamento(Long idEquipamento, Equipamento equipamentoDetails) throws ResourceNotFoundException {
        Equipamento equipamento = contaRepository.findById(idEquipamento)
                .orElseThrow(() -> new ResourceNotFoundException("Equipamento não encontrada para o id:: " + idEquipamento));

        if(equipamentoDetails.getEnergiaKwh() > 0) {
            equipamento.setEnergiaKwh(equipamentoDetails.getEnergiaKwh());
        }
        if(equipamentoDetails.getTipoEquipamento() > 0){
            equipamento.setTipoEquipamento(equipamentoDetails.getTipoEquipamento());
        }

        final Equipamento updatedEquipamento = contaRepository.save(equipamento);
        return updatedEquipamento;
    }

    /**
     *
     * @param idEquipamento
     * @return Equipamento
     * @throws ResourceNotFoundException
     */
    public Equipamento desativarEquipamentoByIdEquipamento(Long idEquipamento) throws ResourceNotFoundException{
        Equipamento equipamento = contaRepository.findById(idEquipamento)
                .orElseThrow(() -> new ResourceNotFoundException("Equipamento não encontrada para o id :: " + idEquipamento));
        equipamento.setFlagAtivo(0L);
        return contaRepository.save(equipamento);
    }

    /**
     *
     * @param idEquipamento
     * @return Equipamento
     * @throws ResourceNotFoundException
     */
    public Equipamento ativarEquipamentoByIdEquipamento(Long idEquipamento) throws ResourceNotFoundException{
        Equipamento equipamento = contaRepository.findById(idEquipamento)
                .orElseThrow(() -> new ResourceNotFoundException("Equipamento não encontrada para o id :: " + idEquipamento));

        equipamento.setFlagAtivo(1L);
        return contaRepository.save(equipamento);

    }

}
