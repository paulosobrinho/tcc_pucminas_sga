package com.tcc.pucminas.sga.service;

import com.tcc.pucminas.sga.model.Transacao;
import com.tcc.pucminas.sga.repository.TransacaoRepository;
import com.tcc.pucminas.sga.interfaceService.ITransacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TransacaoService implements ITransacaoService {

    @Autowired
    TransacaoRepository transacaoRepository;

    /**
     *
     * @param transacao
     * @return Transacao
     */
    public Transacao save(Transacao transacao){
        transacao.setIdTransacao(0);
        return transacaoRepository.save(transacao);
    }

    /**
     *
     * @param idMaterial
     * @return List<Transacao>
     */
    public List<Transacao> findByIdMaterial(long idMaterial){
        return transacaoRepository.findByIdMaterial(idMaterial);
    }

}
