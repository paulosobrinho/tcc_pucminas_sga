package com.tcc.pucminas.sga.service;

import com.tcc.pucminas.sga.exception.ResourceNotFoundException;
import com.tcc.pucminas.sga.interfaceService.IMaterialService;
import com.tcc.pucminas.sga.model.ColetorMaterial;
import com.tcc.pucminas.sga.model.Transacao;
import com.tcc.pucminas.sga.repository.ColetorMaterialRepository;
import com.tcc.pucminas.sga.model.Material;
import com.tcc.pucminas.sga.model.Coletor;
import com.tcc.pucminas.sga.repository.MaterialRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


@Service
public class MaterialService implements IMaterialService {

    @Autowired
    private MaterialRepository contaRepository;

    @Autowired
    private ColetorMaterialRepository coletorMaterialRepository;

    @Autowired
    private  ColetorService coletorService;

    @Autowired
    private  TransacaoService transacaoService;

    public List<Material> findAll(){
        return contaRepository.findAll();
    }

    /**
     *
     * @param idMaterial
     * @return Material
     * @throws ResourceNotFoundException
     */
    public Material findByIdMaterial(Long idMaterial) throws ResourceNotFoundException {
        return  contaRepository.findById(idMaterial)
                .orElseThrow(() -> new ResourceNotFoundException("Material não encontrada para o id: " + idMaterial));
    }

    /**
     *
     * @param idMaterial
     * @param valorDebitar
     * @return String
     * @throws ResourceNotFoundException
     */
    public String saqueByIdMaterial(Long idMaterial, BigDecimal valorDebitar) throws Exception{
        Material material = new Material();
        material = findByIdMaterial(idMaterial);
        if(material.isFlagAtivo().equals(0L)){
            throw new Exception("ERRO ao realizar deposito. A conta " + idMaterial + " não está ativa.");
        }
        if(valorDebitar.compareTo(material.getLimiteSaqueDiario()) == 1){
            throw new Exception("OPERAÇÃO NÃO REALIZADA: Limite de saque diário insuficiente. Valor do limite R$ " + material.getLimiteSaqueDiario().toString());

        }
        if(valorDebitar.compareTo(material.getSaldo()) == 1){
            throw new Exception("OPERAÇÃO NÃO REALIZADA: Saldo  insuficiente. Valor do saldo atual R$ " + material.getSaldo().toString());
        }
        material.setLimiteSaqueDiario(material.getLimiteSaqueDiario().subtract(valorDebitar));
        material.setSaldo(material.getSaldo().subtract(valorDebitar));
        Transacao transacao = new Transacao();
        transacao.setDataTransacao(new Date());
        transacao.setIdTransacao(0);
        transacao.setIdMaterial(idMaterial);
        transacao.setValor(valorDebitar.negate());
        transacao.setDescricao("DEBITO EM CONTA");
        try {
            transacaoService.save(transacao);
        }catch (Exception e){
            throw new Exception("ERRO ao realizar saque.");
        }
        contaRepository.save(material);
        return "O valor foi debitado da conta.";
    }

    /**
     *
     * @param idMaterial
     * @param valorCreditar
     * @return String
     * @throws ResourceNotFoundException
     */
    public String depositoByIdMaterial(Long idMaterial, BigDecimal valorCreditar) throws Exception{
        Material material = new Material();
        material = findByIdMaterial(idMaterial);
        if(material.isFlagAtivo().equals(0L)){
            throw new Exception("ERRO ao realizar deposito. A conta " + idMaterial + " não está ativa.");
        }
        material.setSaldo(material.getSaldo().add(valorCreditar));
        Transacao transacao = new Transacao();
        transacao.setDataTransacao(new Date());
        transacao.setIdTransacao(0);
        transacao.setIdMaterial(idMaterial);
        transacao.setValor(valorCreditar);
        transacao.setDescricao("CREDITO EM CONTA");
        try {
            transacaoService.save(transacao);
        }catch (Exception e){
            throw new Exception("ERRO ao realizar deposito");
        }
        contaRepository.save(material);
        return "O valor foi creditado da conta.";
    }

    /**
     *
     * @param idMaterial
     * @return String
     * @throws ResourceNotFoundException
     */
    public String findSaldoByIdMaterial(Long idMaterial) throws ResourceNotFoundException{
        Material material = new Material();
        material = contaRepository.findById(idMaterial)
                .orElseThrow(() -> new ResourceNotFoundException("Material não encontrada para o id: " + idMaterial));
        Coletor coletor = new Coletor();
        coletor = coletorService.findByIdColetor(material.getIdColetor());
        return "Saldo da conta de  " + coletor.getNome() + " -  R$ " + material.getSaldo().toString();
    }

    /**
     *
     * @param material
     * @return Material
     */
    public Material save(Material material) throws ResourceNotFoundException {
        Coletor coletor = new Coletor();
        coletor = coletorService.findByIdColetor(material.getIdColetor());
        Material materialNova = new Material();
        materialNova =  contaRepository.save(material);
        ColetorMaterial coletorMaterial = new ColetorMaterial();
        coletorMaterial.setIdMaterial(materialNova.getIdMaterial());
        coletorMaterial.setIdColetor(coletor.getIdColetor());
        coletorMaterialRepository.save(coletorMaterial);

        return materialNova;
    }

    /**
     *
     * @param idMaterial
     * @param materialDetails
     * @return Material
     * @throws ResourceNotFoundException
     */
    public Material updateMaterial(Long idMaterial, Material materialDetails) throws ResourceNotFoundException {
        Material material = contaRepository.findById(idMaterial)
                .orElseThrow(() -> new ResourceNotFoundException("Material não encontrada para o id:: " + idMaterial));

        material.setSaldo(materialDetails.getSaldo());
        material.setIdColetor(materialDetails.getIdColetor());
        final Material updatedMaterial = contaRepository.save(material);
        return updatedMaterial;
    }

    /**
     *
     * @param idMaterial
     * @return Material
     * @throws ResourceNotFoundException
     */
    public Material desativarMaterialByIdMaterial(Long idMaterial) throws ResourceNotFoundException{
        Material material = contaRepository.findById(idMaterial)
                .orElseThrow(() -> new ResourceNotFoundException("Material não encontrada para o id :: " + idMaterial));
        material.setFlagAtivo(0L);
        return contaRepository.save(material);
    }

    /**
     *
     * @param idMaterial
     * @return Material
     * @throws ResourceNotFoundException
     */
    public Material ativarMaterialByIdMaterial(Long idMaterial) throws ResourceNotFoundException{
        Material material = contaRepository.findById(idMaterial)
                .orElseThrow(() -> new ResourceNotFoundException("Material não encontrada para o id :: " + idMaterial));

        material.setFlagAtivo(1L);
        return contaRepository.save(material);

    }

}
