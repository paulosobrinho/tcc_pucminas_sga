package com.tcc.pucminas.sga.controller;


import com.tcc.pucminas.sga.interfaceService.IMaterialService;
import com.tcc.pucminas.sga.model.Material;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/sga/equipamento")
@Api(value="Equipamento SGA  - Management System", description="Operaçoes de equipamento e consumo de energia")
public class EquipamentoController {

    @Autowired
    private IMaterialService materialService;

    @ApiOperation(value = "Visualizar a lista de equipamentos", response = List.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Operação Realizada com Sucesso."),
            @ApiResponse(code = 400, message = "Formato incompatível."),
            @ApiResponse(code = 401, message = "Você não possui autorização para acessar este recurso."),
            @ApiResponse(code = 403, message = "Acesso bloqueado ao recurso solicitado."),
            @ApiResponse(code = 404, message = "O recurso que esta tentando acessar não está disponível."),
            @ApiResponse(code = 500, message = "Ocorreu um erro em sua solicitação. Tente mais tarde.")})
    @GetMapping("/listar")
    public List<Material> getAllMaterials() {
        return materialService.findAll();
    }

}
