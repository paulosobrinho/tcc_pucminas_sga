package com.tcc.pucminas.sga.interfaceService;

import com.tcc.pucminas.sga.exception.ResourceNotFoundException;
import com.tcc.pucminas.sga.model.Coletor;

import java.util.List;

public interface IColetorService {

    Coletor save(Coletor coletor) throws Exception;

    Coletor findByIdColetor(Long idColetor) throws ResourceNotFoundException;

    List<Coletor> findByCpfCnpj(String cpfcnpj);

}
